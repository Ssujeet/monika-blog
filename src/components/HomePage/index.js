import React from 'react';

import { useTypewriter } from 'react-simple-typewriter';

import { getPrintWriterText } from '../../../utils';

export const HomePage = ({ detail }) => {
  const { commonText, spliceText } = getPrintWriterText(detail.title);
  const { text } = useTypewriter({
    words: [spliceText],
    loop: 0,
  });

  return (
    <>
      <div
        className="flex flex-col mt-5 pt-5 md:mt-10 md:pt-10"
        // style={{ backgroundImage: `url(${background})` }}
      >
        <h3 className="text-center pt-0 m-0 text-green-400">About Me</h3>
        <h2 className="m-0 p-0 mt-5 text-5xl font-bold text-center">
          {commonText} {text}
        </h2>
        <h2 className="m-0 p-0 text-5xl font-bold text-center">{detail.subText}</h2>
        <p className="self-center md:w-1/2 p-0 m-0 mt-8 text-center">{detail.descriptions}</p>
        <div className="flex justify-center mt-20">
          <a
            href={detail.cv[0].localFile.publicURL}
            download={'Resume'}
            target="_blank"
            rel="noreferrer"
            // onClick={toggleIsReadAll}
            className="border px-12 py-2 rounded-xl bg-green-400 text-white hover:text-white transition delay-150 duration-300 ease-in-out transform hover:-translate-y-1 hover:scale-110 ..."
          >
            Download CV
          </a>
        </div>
      </div>
    </>
  );
};
