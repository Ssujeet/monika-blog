import React from 'react';
import ProjectsCard from './ProjectsCard';

const ProjectsShowCase = ({ allPrismicProjectpage, allPrismicProjects, projectsCount }) => {
  const [singleProjectsPage] = allPrismicProjectpage.nodes;
  const leftSideProjects = allPrismicProjects.nodes.slice(0, Math.ceil(projectsCount / 2));
  const rightSideProjects = allPrismicProjects.nodes.slice(Math.ceil(projectsCount / 2), projectsCount);

  return (
    <div className="flex flex-wrap justify-between">
      <div className="md:w-1/2">
        <div className="flex flex-col">
          <div className="mb-16 md:w-11/12 mt-8">
            <h2 className="text-3xl font-bold">{singleProjectsPage.data.title.text}</h2>
            <p className="text-lg mt-4">{singleProjectsPage.data.descriptions}</p>
          </div>

          {rightSideProjects.map((item) => (
            <ProjectsCard project={item} />
          ))}
        </div>
      </div>

      <div className="md:w-1/2">
        <div className="flex flex-col flex-end">
          {leftSideProjects.map((item) => (
            <div className="flex justify-end">
              <ProjectsCard project={item} />
            </div>
          ))}
        </div>
      </div>
    </div>
  );
};

export default ProjectsShowCase;
