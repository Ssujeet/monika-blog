import React from 'react';
import { Link } from 'gatsby';
import { GatsbyImage } from 'gatsby-plugin-image';
const ProjectsCard = ({ project }) => {
  const { title, islive, descriptions, bannerimage, repourl, projectlink } = project.data;
  return (
    <div className="w-full md:w-11/12 bg-slate-50 mt-8">
      <GatsbyImage image={bannerimage.gatsbyImageData} alt={`Hero image`} />

      <div className="p-8 md:p-12">
        <h1 className="text-3xl font-bold">{title.text}</h1>
        <p className="text-lg">{descriptions}</p>
        {repourl.url && (
          <Link to={repourl.url} className="mt-4 text-base font-bold left-icon block hover:opacity-20">
            See Code Url
          </Link>
        )}
        {islive && projectlink.url && (
          <Link to={projectlink.url} className="mt-1 text-base font-bold left-icon hover:opacity-20">
            View Live
          </Link>
        )}
      </div>
    </div>
  );
};

export default ProjectsCard;
