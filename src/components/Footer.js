import * as React from 'react';

import prismicLogo from '../images/logo-prismic.svg';
import { AiFillHome, AiFillContacts, AiFillProject } from 'react-icons/ai';
import { HiDocumentSearch } from 'react-icons/hi';
import { FaServicestack, FaGitlab, FaLinkedin } from 'react-icons/fa';
import { FiMail } from 'react-icons/fi';
import { motion } from 'framer-motion';
import { Link, useStaticQuery } from 'gatsby';

const iconProperties = {
  size: 30,
  color: 'gray',
};
export const Footer = ({ allPrismicPost, allPrismicSkillspage, singleHomePage }) => {
  const [singleSkillspage] = allPrismicSkillspage?.nodes;
  return (
    <footer className="container">
      <div className="flex flex-wrap justify-between">
        <div className="w-1/2 md:w-1/4">
          <div className="flex">
            <h1 className="text-xl font-bold">Monika Prajapati</h1>
          </div>

          <div className="mt-2">
            <ul className="flex">
              <li className="pointer mr-4 hover:opacity-50">
                <a href={singleHomePage?.data?.githuburl?.url} target="_blank" rel="noreferrer">
                  <FaGitlab size={iconProperties.size} color={iconProperties.color} />
                </a>
              </li>
              <li className="pointer mr-4 hover:opacity-50">
                <a href={singleHomePage?.data?.linkedinurl?.url} target="_blank" rel="noreferrer">
                  <FaLinkedin size={iconProperties.size} color={iconProperties.color} />
                </a>
              </li>

              <li className="pointer mr-4 hover:opacity-50">
                <a href={`mailto:${singleHomePage?.data?.email}`} target="_blank" rel="noreferrer">
                  <FiMail size={iconProperties.size} color={iconProperties.color} />
                </a>
              </li>
            </ul>
          </div>
          <p className="mt-2 text-sm">
            You can also contact by sending me message{' '}
            <Link to="/contact" className="text-blue-300">
              here
            </Link>
          </p>
        </div>
        <FooterBlock className={''} title={'Technologies'}>
          {singleSkillspage.data?.skillslists?.map((item) => (
            <p className="text-left text-sm">{item.skillstitle.text}</p>
          ))}
        </FooterBlock>
        <FooterBlock className={''} title={'Useful Links'}>
          <Link to={'/blogs'} className="text-left text-sm block hover:text-blue-400">
            Blogs
          </Link>
          <Link to="/contact" className="text-left text-sm block  hover:text-blue-400">
            Contact
          </Link>
        </FooterBlock>
        <FooterBlock className={'text-left'} title={'Latest Blogs'}>
          {allPrismicPost.nodes.map((item) => {
            return (
              <Link to={item.url} className=" text-sm block">
                {item.data.title.text}
              </Link>
            );
          })}
        </FooterBlock>
      </div>

      <p className="mt-4 border-top">
        Copyright © {new Date().getFullYear()} Monika Prajapati
        <br />
      </p>
    </footer>
  );
};

const FooterBlock = ({ className, title, children }) => {
  return (
    <div className={`w-1/2 md:w-1/4 ${className}`}>
      <h1 className="text-xl font-bold">{title}</h1>
      {children}
    </div>
  );
};
