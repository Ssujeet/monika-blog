import React from 'react';
import { AiFillHome } from 'react-icons/ai';
import { AiFillMail, AiFillPhone } from 'react-icons/ai';
const ContactDetail = ({ detail }) => {
  return (
    <div>
      <div class="grid grid-cols-1 divide-y-8 divide-x-0 divide-green-500">
        <div>
          <h2 className="text-7xl">{detail?.title}</h2>
          <p>{detail?.subText}</p>
        </div>
        <div className="pt-5 pl-0 ml-0">
          <ul className="pl-0">
            <li className="flex">
              <AiFillHome />
              <span className="ml-4">{detail?.address}</span>
            </li>
            <li className="flex">
              <a href={`mailto:${detail?.email}`} className="flex">
                <AiFillMail />
                <span className="ml-4">{detail?.email}</span>
              </a>
            </li>
            <li>
              <a href={`tel:${detail?.phoneNumber}`} className="flex">
                <AiFillPhone />
                <span className="ml-4">{detail?.phoneNumber}</span>
              </a>
            </li>
          </ul>
        </div>
      </div>
      <div class="flex flex-col-reverse divide-y divide-y-reverse divide-rose-400"></div>
    </div>
  );
};

export default ContactDetail;
