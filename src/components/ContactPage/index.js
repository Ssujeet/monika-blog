import React from 'react';
import { graphql, useStaticQuery } from 'gatsby';

import ContactForm from './contact-form';

const ContactPage = () => {
  return (
    <div className="pt-12 ">
      <div className="md:flex md:flex-row  md:justify-evenly pt-12">
        <div className="md:w-1/2">
          <h1 className="text-3xl font-bold">Send me a message</h1>
          <ContactForm />
        </div>
      </div>
    </div>
  );
};

export default ContactPage;
