import React from 'react';
import { useFormik } from 'formik';
import { toast } from 'react-toastify';
import * as Yup from 'yup';
const phoneRegExp =
  /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/;

const messageSchema = Yup.object().shape({
  message: Yup.string().required('Message is Required'),
  fullName: Yup.string().required('Full Name is Required'),
  email: Yup.string().email('Invalid email').required('Email is Required'),
});

const initialValues = {
  fullName: '',
  email: '',
  message: '',
  number: '',
  file: [],
};
const ContactInformationsForm = ({ propertyDetails }) => {
  const [files, setFiles] = React.useState([]);

  const formik = useFormik({
    initialValues: { ...initialValues },
    validationSchema: messageSchema,

    onSubmit: (values) => {
      onSubmit(values);
    },
  });

  const {
    values,
    handleChange,
    isSubmitting,
    setSubmitting,
    errors,
    touched,
    handleSubmit,
    setValues,
    handleBlur,
    resetForm,
  } = formik;

  const onSubmit = (values) => {
    let formData = new FormData();
    formData.append('message', values.message);
    formData.append('fullName', values.fullName);
    formData.append('email', values.email);

    const data = {
      username: 'UserRequest',
      avatar_url: '',
      content: `[Portfolio Message] \nMessage sent by ${values.fullName} \nEmail: ${values.email} \nMessage : ${values.message} `,
    };

    fetch(
      'https://discord.com/api/webhooks/925804361895006248/Lk3X3O1HQKVTfs1tlUxMjWEPTINr_BR6BmFRHdXE2rqgwnM8kCaVU3Dkm9sln2V7ORx3',
      {
        // Adding method type
        method: 'POST',

        // Adding body or contents to send
        body: JSON.stringify(data),

        headers: {
          'Content-type': 'application/json; charset=UTF-8',
        },
      }
    )
      .then((response) => {
        toast.success('Your message has been send successfully.');
        setSubmitting(false);
        resetForm();
        return response.json();
      })
      .then((json) => {
        setSubmitting(false);
        resetForm();
        console.log(json);
      })
      .catch((err) => {
        toast.warn('Some server occured during sending message');
      });
  };

  const hasError = (property) => {
    return errors && errors[property] && touched && touched[property];
  };

  return (
    <div className="">
      <form class="w-full" onSubmit={handleSubmit}>
        <div class="items-center border-b border-gray-500 py-2 w-full">
          <input
            class={`appearance-none bg-transparent border-none w-full text-gray-700 mr-3 py-1 px-2 leading-tight focus:outline-none ${
              hasError('fullName') && 'outline-red-700'
            }`}
            type="text"
            placeholder="Full Name"
            aria-label="Full name"
            name="fullName"
            value={values.fullName}
            onChange={handleChange}
            onBlur={handleBlur}
          />
        </div>
        {hasError('fullName') && <span className="px-2 text-red-700">{errors['fullName']}</span>}

        <div class="items-center border-b border-gray-500 py-2 mt-5">
          <input
            class="appearance-none bg-transparent border-none w-full text-gray-700 mr-3 py-1 px-2 leading-tight focus:outline-none"
            type="text"
            placeholder="Email"
            aria-label="Email"
            name="email"
            value={values.email}
            onChange={handleChange}
            onBlur={handleBlur}
          />
        </div>
        {hasError('email') && <span className="px-2 text-red-700">{errors['email']}</span>}

        <div class="items-center border-b border-gray-500 py-2 mt-5">
          <input
            class="appearance-none bg-transparent border-none w-full text-gray-700 mr-3 py-1 px-2 leading-tight focus:outline-none"
            type="textarea"
            placeholder="Your Message"
            aria-label="Full name"
            name="message"
            value={values.message}
            onChange={handleChange}
            onBlur={handleBlur}
          />
        </div>
        {hasError('message') && <span className="px-2 text-red-700">{errors['message']}</span>}

        <div className="mt-10 flex justify-center md:justify-start">
          <button
            className="border py-4 px-10  bg-blue-400 hover:bg-blue-500 text-white transition delay-150 duration-300 ease-in-out transform hover:-translate-y-1 hover:scale-110 ..."
            type="submit"
            disabled={isSubmitting}
          >
            {isSubmitting ? 'Loading...' : 'Send Message'}
          </button>
        </div>
      </form>
    </div>
  );
};

export default ContactInformationsForm;
