import React from 'react';
import { Link } from 'gatsby';

const NavBar = ({ differentColor }) => {
  const bgColor = !differentColor ? 'bg-gray-50' : '';
  const linkColor = !differentColor ? '' : 'text-white';

  return (
    <div className="fixed top-0 w-full " style={{ zIndex: 2000 }}>
      <div className={`${bgColor} ${linkColor}`}>
        <div className="py-4 container">
          <div className="flex justify-between">
            <div className="w-1/3 ">
              <Link className="text-3xl font-bold" to={'/'}>
                мσиιкα{/* <img src={Logo} height="3" className="" style={{ height: '60px' }} /> */}
              </Link>
            </div>
            <div className="text-center">
              <Link to={'/#projects-section'} className="mx-4 cursor-pointer hover:text-blue-500  text-lg">
                Projects
              </Link>
              <Link to={'/#skills-section'} className="mx-4 cursor-pointer hover:text-blue-500  text-lg">
                Skills
              </Link>
              <Link to={'/blogs'} className="mx-4 cursor-pointer hover:text-blue-500  text-lg">
                Blogs
              </Link>
              <Link to={'/contact'} className="mx-4 cursor-pointer hover:text-blue-500  text-lg">
                Contact
              </Link>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default NavBar;
