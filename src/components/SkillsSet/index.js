import React from 'react';

const SkillsSet = ({ allPrismicSkillspage }) => {
  const [singleSkillsPage] = allPrismicSkillspage.nodes;
  return (
    <div>
      <div className="flex justify-between">
        <div className="w-5/12">
          <h1 className="text-4xl">{singleSkillsPage.data.title.text}</h1>
          <p className="text-lg mt-8">{singleSkillsPage.data.metadescriptions}</p>
        </div>
        <div className="w-1/2 flex flex-wrap">
          {singleSkillsPage.data.skillslists.map((item) => {
            return <SkillBadge text={item.skillstitle.text} />;
          })}
        </div>
      </div>
    </div>
  );
};

export default SkillsSet;

const SkillBadge = ({ text }) => <span className="text-xl bg-white rounded-3xl px-12 py-2 m-3"> {text}</span>;
