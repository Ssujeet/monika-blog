import React from 'react';
import { Link } from 'gatsby';
import { firstParagraph } from '../BlogPosts';
import { GatsbyImage } from 'gatsby-plugin-image';
const BlogsCard = ({ posts }) => {
  const { title, date, thumbNailImage, descriptions, body } = posts.data;
  return (
    <div className="md:w-1/2 lg:w-1/3 pr-3 my-4   ">
      <Link to={posts.url}>
        <div className=" drop-shadow-2xl flex bg-neutral-50 py-16  px-5 pt-5 justify-between transform transition duration-500 hover:scale-105 cursor-pointer border ">
          <div className="w-8/12 self-center h-full py-6">
            <h1 className="text-xl font-bold">{title.text}</h1>
            <p className="mt-4 text-base">{descriptions}</p>
            <p className="text-sm text-gray-500 mt-2">{date}</p>
            <div className="mt-4">
              <Link className="text-sm font-bold left-icon">Learn More</Link>
            </div>
          </div>
          <div className="w-1/3 h-full">
            <GatsbyImage image={thumbNailImage?.gatsbyImageData} alt={`Hero image`} />
          </div>
        </div>
      </Link>
    </div>
  );
};

export default BlogsCard;
