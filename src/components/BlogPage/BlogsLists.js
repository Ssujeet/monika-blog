import React from 'react';
import BlogsCard from './BlogsCard';

const BlogsLists = ({ allPrismicPost }) => {
  return (
    <div>
      <div className="flex justify-between flex-wrap">
        {allPrismicPost.nodes.map((item) => (
          <BlogsCard posts={item} />
        ))}
      </div>
    </div>
  );
};

export default BlogsLists;
