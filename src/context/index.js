import React, { createContext, useContext, useState } from 'react';

export const ThemeContext = createContext();

function Context({ children }) {
  const [isOpen, setIsOpen] = useState(false);

  return (
    <ThemeContext.Provider
      value={{
        isOpen,
        setIsOpen,
      }}
    >
      {children}
    </ThemeContext.Provider>
  );
}

export default Context;
