export const getPrintWriterText = (text) => {
  const commonTextList = text.split(' ');
  const spliceText = commonTextList.splice(commonTextList.length - 2);
  return {
    commonText: commonTextList.join(' '),
    spliceText: spliceText.join(' '),
  };
};
