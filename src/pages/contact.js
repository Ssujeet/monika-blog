import * as React from 'react';
import { withPrismicPreviewResolver } from 'gatsby-plugin-prismic-previews';
import ContactPage from '../components/ContactPage';
import { graphql } from 'gatsby';
import { Footer } from '../components/Footer';
import NavBar from '../components/NavBar';
export const query = graphql`
  query ContactPageQuery {
    allPrismicHomepage {
      nodes {
        data {
          descriptions
          email
          githuburl {
            url
          }
          linkedinurl {
            url
          }
          logo {
            gatsbyImageData(width: 10, height: 10)
          }
          phonenumber
          resumeurl {
            url
          }
          title {
            text
          }
        }
      }
    }
    allPrismicSkillspage {
      nodes {
        data {
          skillslists {
            skillstitle {
              text
            }
          }
        }
      }
    }
    allPrismicPost(sort: { fields: data___date, order: DESC }, limit: 3) {
      nodes {
        _previewable
        url
        data {
          title {
            text
          }
        }
      }
    }
  }
`;
const Contact = ({ isPreview, data }) => {
  if (isPreview === false) return 'Not a preview!';
  if (!data) return null;
  const { allPrismicHomepage, allPrismicSkillspage, allPrismicPost } = data;

  return (
    <>
      <NavBar />
      <div className="container mx-auto">
        <div className="flex flex-col justify-center" style={{ height: '70vh' }}>
          <ContactPage />
        </div>
      </div>
      <div className=" container margin-auto mb-16 ">
        <Footer
          allPrismicPost={allPrismicPost}
          allPrismicSkillspage={allPrismicSkillspage}
          singleHomePage={allPrismicHomepage.nodes[0]}
        />
      </div>
    </>
  );
};

export default withPrismicPreviewResolver(Contact);
