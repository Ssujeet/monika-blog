import React from 'react';
import BlogsLists from '../components/BlogPage/BlogsLists';
import SkillsSet from '../components/SkillsSet';
import { Link, graphql } from 'gatsby';
import ProjectsShowCase from '../components/ProjectsShowCase';
import { Footer } from '../components/Footer';
import NavBar from '../components/NavBar';

const HomePage = ({ data }) => {
  const { allPrismicSkillspage, allPrismicPost, allPrismicProjects, allPrismicProjectpage, allPrismicHomepage } = data;
  const [isDifferentColor, setIsDifferentColor] = React.useState(true);
  const [singleHomePage] = allPrismicHomepage.nodes;

  const isBrowser = typeof window !== 'undefined';
  isBrowser &&
    window.addEventListener('scroll', (event) => {
      setIsDifferentColor(window.scrollY < 80);
    });

  return (
    <div>
      <NavBar differentColor={isDifferentColor} />
      <div className="home-container">
        <div className="container mx-auto flex flex-col justify-center h-full ">
          <div>
            <h1 className="text-gray-800 w-4/6 text-2xl sm:text-5xl md:text-7xl font-bold ">{singleHomePage.data.title.text}</h1>
            <div>
              <p className="my-4">To get to know more about me..</p>
              <a
                href={singleHomePage.data.resumeurl.url}
                download={singleHomePage.data.resumeurl.url}
                className="bg-white rounded-lg px-8 py-2 mt-12  hover:text-purple-900 hover:bg-neutral-60 transform transition duration-500 hover:scale-105"
                target="_blank"
              >
                View CV
              </a>
            </div>
          </div>
          <div></div>
        </div>
        <div style={{ marginTop: -200 }}>
          <div className="container mx-auto">
            <div className="flex  justify-between w-full ">
              <h1 className="text-4xl font-bold my-5 ">Latest Blogs</h1>

              <Link to="/blogs" className="text-white flex self-center hover:text-blue-200">
                View All
              </Link>
            </div>
            <BlogsLists allPrismicPost={allPrismicPost} />
          </div>
        </div>
        <div id="projects-section" className="mt-4 md:p-12">
          <div className=" container margin-auto mb-16 ">
            <ProjectsShowCase
              allPrismicProjects={allPrismicProjects}
              allPrismicProjectpage={allPrismicProjectpage}
              projectsCount={allPrismicProjects.totalCount}
            />
          </div>
        </div>
        <div id="skills-section" className="skills-container p-12">
          <div className=" container margin-auto mb-16 ">
            <SkillsSet allPrismicSkillspage={allPrismicSkillspage} />
          </div>
        </div>
        <div className=" container margin-auto mb-16 ">
          <Footer
            allPrismicPost={allPrismicPost}
            allPrismicSkillspage={allPrismicSkillspage}
            singleHomePage={singleHomePage}
          />
        </div>
      </div>
    </div>
  );
};

export default HomePage;

export const query = graphql`
  query HomePageQuery {
    allPrismicHomepage {
      nodes {
        data {
          descriptions
          email
          githuburl {
            url
          }
          linkedinurl {
            url
          }
          logo {
            gatsbyImageData(width: 10, height: 10)
          }
          phonenumber
          resumeurl {
            url
          }
          title {
            text
          }
        }
      }
    }

    allPrismicSkillspage {
      nodes {
        data {
          skillslists {
            skillsdescriptions
            skillstitle {
              text
            }
          }
          title {
            text
          }
          metadescriptions
        }
      }
    }
    allPrismicProjects {
      nodes {
        data {
          projectlink {
            uid
            url
          }
          repourl {
            uid
            url
          }
          title {
            text
          }
          islive
          descriptions
          bannerimage {
            gatsbyImageData(width: 834, height: 562)
          }
        }
      }
      totalCount
    }
    allPrismicProjectpage {
      nodes {
        data {
          title {
            text
          }
          descriptions
        }
      }
    }
    allPrismicPost(sort: { fields: data___date, order: DESC }, limit: 3) {
      nodes {
        _previewable
        id
        url
        data {
          title {
            text
          }
          date(formatString: "MMM D, YYYY")
          thumbNailImage: thumbnail_image {
            gatsbyImageData(width: 300, height: 400)
          }
          body {
            ... on PrismicPostDataBodyText {
              id
              slice_label
              slice_type
              primary {
                text {
                  richText
                }
              }
            }
          }
          descriptions
        }
      }
    }
  }
`;
