import * as React from 'react';
import { withPrismicUnpublishedPreview } from 'gatsby-plugin-prismic-previews';
import NavBar from '../components/NavBar';
import { Footer } from '../components/Footer';
import { graphql } from 'gatsby';
export const query = graphql`
  query PageNoFoundQuery {
    allPrismicHomepage {
      nodes {
        data {
          descriptions
          email
          githuburl {
            url
          }
          linkedinurl {
            url
          }
          logo {
            gatsbyImageData(width: 10, height: 10)
          }
          phonenumber
          resumeurl {
            url
          }
          title {
            text
          }
        }
      }
    }
    allPrismicSkillspage {
      nodes {
        data {
          skillslists {
            skillstitle {
              text
            }
          }
        }
      }
    }
    allPrismicPost(sort: { fields: data___date, order: DESC }, limit: 3) {
      nodes {
        _previewable
        url
        data {
          title {
            text
          }
        }
      }
    }
  }
`;
const NotFoundPage = ({ data }) => {
  if (!data) return null;
  const { allPrismicHomepage, allPrismicSkillspage, allPrismicPost } = data;
  return (
    <>
      <NavBar />
      <div className="not-found">
        <h1>404</h1>
        <h3>The page you are looking for was not found</h3>
        <p>
          <a href="/">
            <button type="button">Return to homepage</button>
          </a>
        </p>
      </div>
      <Footer
        allPrismicPost={allPrismicPost}
        allPrismicSkillspage={allPrismicSkillspage}
        singleHomePage={allPrismicHomepage.nodes[0]}
      />
    </>
  );
};

export default withPrismicUnpublishedPreview(NotFoundPage);
