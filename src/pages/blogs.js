import * as React from 'react';
import { graphql } from 'gatsby';
import { withPrismicPreview } from 'gatsby-plugin-prismic-previews';
import { BlogPosts } from '../components/BlogPosts';
import BlogsLists from '../components/BlogPage/BlogsLists';
import NavBar from '../components/NavBar';

export const query = graphql`
  query MyqueryBlogs {
    prismicBloghome {
      _previewable
      data {
        description {
          text
        }
        headline {
          text
        }
        image {
          url
        }
      }
    }
    allPrismicPost(sort: { fields: data___date, order: DESC }) {
      nodes {
        _previewable
        id
        url
        data {
          title {
            text
          }
          date(formatString: "MMM D, YYYY")
          thumbNailImage: thumbnail_image {
            gatsbyImageData(width: 300, height: 400)
          }
          descriptions
          body {
            ... on PrismicPostDataBodyText {
              id
              slice_label
              slice_type
              primary {
                text {
                  richText
                }
              }
            }
          }
        }
      }
      pageInfo {
        currentPage
        pageCount
      }
    }
  }
`;

const Blogs = ({ data }) => {
  if (!data) return null;
  const home = data.prismicBloghome.data;
  const docs = data.allPrismicPost;

  const avatar = { backgroundImage: `url(${home.image.url})` };

  return (
    <>
      <NavBar />
      <div className="container margin-auto pt-12">
        <h1 className="text-3xl font-bold pt-12 ">Read from Our Blogs</h1>
        <BlogsLists allPrismicPost={docs} />
      </div>
    </>
  );
};

export default withPrismicPreview(Blogs);
