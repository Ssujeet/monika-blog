import * as React from 'react';
import { graphql, Link } from 'gatsby';
import { SliceZone } from '@prismicio/react';
import { withPrismicPreview } from 'gatsby-plugin-prismic-previews';
import { components } from '../slices';
import NavBar from '../components/NavBar';

export const query = graphql`
  query BlogPostQuery($id: String) {
    prismicPost(id: { eq: $id }) {
      _previewable
      id
      uid
      lang
      type
      url
      data {
        date
        title {
          text
        }
        body {
          ... on PrismicSliceType {
            id
            slice_label
            slice_type
          }
          ...PostDataBodyText
          ...PostDataBodyQuote
          ...PostDataBodyImageWithCaption
        }
      }
    }
  }
`;

const Post = ({ data }) => {
  if (!data) return null;

  const post = data.prismicPost.data;
  const title = post.title.text || 'Untitled';

  return (
    <>
      <div className="bg-gray-100">
        <NavBar />
      </div>
      <div className="bg-gray-100 py-12">
        <div className="container post-header bg-white mt-14  ">
          <div className="px-3 md:px-16 py-8">
            <div className="back">
              <Link className="font-bold" to="/">
                Back to list
              </Link>
            </div>
            <h1 className="my-2 font-bold text-4xl">{title}</h1>
            <SliceZone slices={post.body} components={components} />
          </div>
        </div>
      </div>
    </>
  );
};
export default withPrismicPreview(Post);
