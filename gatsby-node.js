const path = require('path');

exports.createPages = async ({ graphql, actions }) => {
  const { createPage } = actions;

  const postPages = await graphql(`
    {
      allPrismicPost {
        nodes {
          id
          url
        }
      }
    }
  `);

  postPages.data.allPrismicPost.nodes.forEach((page) => {
    createPage({
      path: page.url,
      component: path.resolve(__dirname, 'src/templates/post.js'),
      context: {
        id: page.id,
      },
    });
  });
};
