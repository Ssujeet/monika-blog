import * as React from 'react';
import { Link } from 'gatsby';
import { PrismicProvider } from '@prismicio/react';
import { PrismicPreviewProvider } from 'gatsby-plugin-prismic-previews';

import { repositoryConfigs } from './src/utils/prismicPreviews';
import Context from './src/context';

import './src/stylesheets/main.scss';
import './src/stylesheets/global.css';
import NavBar from './src/components/NavBar';

export const wrapRootElement = ({ path, element }) => (
  <PrismicProvider internalLinkComponent={({ href, ...props }) => <Link to={href} {...props} />}>
    <PrismicPreviewProvider repositoryConfigs={repositoryConfigs}>
      <Context> {element}</Context>
    </PrismicPreviewProvider>
  </PrismicProvider>
);
